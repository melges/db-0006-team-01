package com.db.server;


import com.db.domain.MessageTransport;
import com.db.exceptions.MessageTransportException;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Student on 29.04.2015.
 */
public class ClientSession implements Closeable, Comparable<ClientSession> {
    private static final Logger LOGGER = Logger.getLogger(ClientSession.class.getName());

    private final Object syncWriteMonitor = new Object();

    private final Object syncReadMonitor = new Object();

    private final Closeable originStream;

    private final ObjectOutputStream outputStream;

    private final ObjectInputStream inputStream;

    private volatile String clientName;

    public ClientSession(Socket originStream)
            throws IOException {
        this.originStream = originStream;
        this.outputStream = new ObjectOutputStream(originStream.getOutputStream());
        this.inputStream = new ObjectInputStream(originStream.getInputStream());
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void sendMessageToClient(MessageTransport messageToWrite) throws IOException {
        synchronized (syncWriteMonitor) {
            outputStream.writeObject(messageToWrite);
        }
    }

    public void sendMessagesToClient(List<MessageTransport> messagesToSend) throws IOException {
        synchronized (syncWriteMonitor) {
            for(MessageTransport message : messagesToSend) {
                outputStream.writeObject(message);
                outputStream.reset();
                outputStream.flush();
            }
        }
    }

    public MessageTransport receiveMessageFromClient() throws IOException, MessageTransportException {
        try {
            synchronized (syncReadMonitor) {
                return (MessageTransport) inputStream.readObject();
            }
        } catch (ClassNotFoundException | ClassCastException e) {
            LOGGER.warning("Some client send invalid data: " + e.getMessage());
            throw new MessageTransportException("Client send invalid data", e);
        }
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
        outputStream.close();
        originStream.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientSession)) {
            return false;
        }

        ClientSession that = (ClientSession) o;

        return !(clientName != null ? !clientName.equals(that.clientName) : that.clientName != null);
    }

    @Override
    public int hashCode() {
        return clientName != null ? clientName.hashCode() : 0;
    }

    @Override
    public int compareTo(ClientSession o) {
        return this.clientName.compareTo(o.clientName);
    }
}
