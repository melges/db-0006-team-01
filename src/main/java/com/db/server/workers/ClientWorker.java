package com.db.server.workers;

import com.db.domain.Command;
import com.db.domain.MessageTransport;
import com.db.exceptions.CommandNotFoundException;
import com.db.exceptions.MessageTransportException;
import com.db.server.commands.AbstractCommand;
import com.db.server.ChatServer;
import com.db.server.ClientSession;
import com.db.server.commands.CommandFactory;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Student on 29.04.2015.
 */
public class ClientWorker implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(ClientWorker.class.getName());

    private final ClientSession clientSession;
    private final ChatServer parentServer;

    public ClientWorker(ClientSession session, ChatServer parentServer) {
        clientSession = session;
        this.parentServer = parentServer;
    }

    private void greetClient() throws IOException {
        clientSession.sendMessageToClient(new MessageTransport(null, "Welcome, use '/chid' for login"));
    }

    private void registerClient() throws IOException, MessageTransportException {
        MessageTransport message = clientSession.receiveMessageFromClient();
        if(message.getServerCommand() != Command.LOGIN) {
            clientSession.sendMessageToClient(new MessageTransport(null, "You do not meet protocol"));
            throw new MessageTransportException("Client don't meet protocol");
        }
        clientSession.setClientName(message.getArgument());
        parentServer.addClient(clientSession);
    }

    @Override
    public void run() {
        try (ClientSession session = clientSession) {
            greetClient();
            registerClient();
            while (!Thread.interrupted()) {
                MessageTransport message;
                AbstractCommand command;
                message = session.receiveMessageFromClient();
                try {
                    command = CommandFactory.createCommand(message, parentServer, session);
                    command.execute();
                } catch (CommandNotFoundException e) {
                    LOGGER.log(Level.INFO, "Unknown command", e);
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Could not Write to client!", e);
        } catch (MessageTransportException e) {
            LOGGER.log(Level.WARNING, "Incorrect message received", e);
        } finally {
            parentServer.removeClient(clientSession);
        }
    }
}
