package com.db.server.workers;

import com.db.domain.MessageTransport;
import com.db.server.ChatServer;
import com.db.server.ClientSession;
import com.db.server.FileHistory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by Student on 29.04.2015.
 */
public class ServerWriter extends Thread {
    private static final Logger LOGGER = Logger.getLogger(ServerWriter.class.getName());

    private static final int MAX_WRITER_THREADS_NUM = 13;

    private final BlockingQueue<MessageTransport> messageToSendQueue =
            new ArrayBlockingQueue<MessageTransport>(MAX_WRITER_THREADS_NUM * 15);
    private final FileHistory history;

    /**
     * Set of clients are responsible for the server
     */
    private final ConcurrentSkipListSet<ClientSession> respondClients;

    private final ChatServer parentServer;

    private final ExecutorService writersExecuteService = Executors.newFixedThreadPool(MAX_WRITER_THREADS_NUM);

    public ServerWriter(ConcurrentSkipListSet<ClientSession> respondClients, ChatServer parentServer, FileHistory history) {
        this.respondClients = respondClients;
        this.parentServer = parentServer;
        this.history = history;
        this.history.init();
    }

    /**
     * Class which will write to clients all received messages in separate threads
     */
    class Writer implements Runnable {

        private final int myNumber;
        private final List<MessageTransport> messages;

        public Writer(int threadNumber, List<MessageTransport> messages) {
            myNumber = threadNumber;
            this.messages = messages;
        }

        @Override
        public void run() {
            for (ClientSession session : respondClients) {
                try {
                    if ((Math.abs(session.hashCode()) % MAX_WRITER_THREADS_NUM) != myNumber) {
                        continue;
                    }

                    session.sendMessagesToClient(messages);
                } catch (IOException e) {
                    LOGGER.warning("Error on sending messages to client: ");
                }
            }
        }

    }
    private List<MessageTransport> getTask() throws InterruptedException {
        List<MessageTransport> tasks = new ArrayList<>();

        tasks.add(messageToSendQueue.take());

        while(messageToSendQueue.size() > 0 && tasks.size() < 4096) {
            tasks.add(messageToSendQueue.take());
        }

        return tasks;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                List<MessageTransport> task = getTask();
                for (MessageTransport message : task) {
                    history.addToHistory(message.getArgument());
                }

                for(int i = 0; i < MAX_WRITER_THREADS_NUM; i++) {
                    writersExecuteService.execute(new Writer(i, task));
                }
            }
        } catch (InterruptedException e) {
            LOGGER.info("We was interrupted, exiting");
        }

        history.deinit();
    }

    public void addMessage(MessageTransport messageTransport) throws InterruptedException {
        messageToSendQueue.put(messageTransport);
    }

    public String getHistory() {
        return history.getFullHistory();
    }
}
