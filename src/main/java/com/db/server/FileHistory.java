package com.db.server;

import java.io.*;
import java.util.logging.Logger;

/**
 *  Implements history handler.
 *  This class isn't synchronized.
 *
 *  @author ESadikov
 */
public class FileHistory {
    private static final Logger log = Logger.getLogger(FileHistory.class.getName());
    private static final String historyFileExt = ".txt";
    private static final String defaultEncoding = "UTF-8";
    private final String historyFile;
    BufferedWriter writer = null;

    public FileHistory(String roomName) {
        historyFile = roomName + historyFileExt;
    }

    /**
     * Initializes history handler.
     * Caller must call this method before calling other method of this class.
     *
     * @return true if history handler was initialized, false - otherwise
     */
    public synchronized boolean init() {
        if (null == writer) {
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(historyFile, true), defaultEncoding));
            } catch (FileNotFoundException e) {
                log.info("Can't open history file " + historyFile + " . Error: " + e.getMessage());
            } catch (UnsupportedEncodingException e) {
                log.info("Unsupported encoding: " + defaultEncoding + ". " + e.getMessage());
            }
        }

        return (writer != null);
    }

    /**
     * Adds message to the history.
     *
     * @param message message to add
     * @throws java.lang.IllegalStateException if history handler isn't ready to save history
     */
    public synchronized void addToHistory(String message) {
        if (null == writer) throw new IllegalStateException("history file wasn't opened");

        try {
            writer.write(message + "\n");
            writer.flush();
        } catch (IOException e) {
            log.info("Can't add message '" + message + "' to history. Error: " + e.getMessage());
        }
    }

    public synchronized String getFullHistory() {
        StringBuilder history = new StringBuilder();

        deinit();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(historyFile), defaultEncoding))) {
            String line;
            while (null != (line = reader.readLine())) {
                history.append(line);
                history.append('\n');
            }
        } catch (FileNotFoundException e) {
            log.info("No history file. Error: " + e.getMessage());
        } catch (IOException e) {
            log.info("IO error: " + e.getMessage());
        }

        init();

        return history.toString();
    }

    /**
     * Deinitializes history handler.
     * Caller must call this method before exit in order to free system resources.
     */
    public synchronized void deinit() {
        if (writer != null) {
            try {
                writer.flush();
                writer.close();
                writer = null;
            } catch (IOException e) {
                log.info("Error at deinit: " + e.getMessage());
            }
        }
    }
}
