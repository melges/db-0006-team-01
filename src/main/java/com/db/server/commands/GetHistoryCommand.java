package com.db.server.commands;

import com.db.domain.MessageTransport;
import com.db.server.ChatServer;
import com.db.server.ClientSession;

import java.io.IOException;

/**
 * Get History command.
 *
 * @author ABogdanov
 * @author DNazarov
 * @author DPogorelov
 */
public class GetHistoryCommand extends AbstractCommand {
    private final ClientSession session;
    private final ChatServer server;

    GetHistoryCommand(MessageTransport messageTransport, ClientSession session, ChatServer server) {
        super(messageTransport);
        this.session = session;
        this.server = server;
    }

    /**
     * History commands retrieves full history
     * from the server and returns it to the client
     */
    @Override
    public void execute() throws IOException{
        String history = server.getHistory();
        messageTransport.setArgument(history);
        session.sendMessageToClient(messageTransport);
    }
}
