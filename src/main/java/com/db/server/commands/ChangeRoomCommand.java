package com.db.server.commands;

import com.db.domain.MessageTransport;
import com.db.server.ClientSession;

import java.io.IOException;

/**
 * @class ChangeRoomCommand
 * @see com.db.server.commands.AbstractCommand
 *
 * Change room command.
 */
public class ChangeRoomCommand extends AbstractCommand {
    private final ClientSession session;

    ChangeRoomCommand(MessageTransport messageTransport, ClientSession session) {
        super(messageTransport);
        this.session = session;
    }

    @Override
    public void execute() throws IOException{
        messageTransport.setArgument("Command isn't supported");
        session.sendMessageToClient(messageTransport);
    }
}
