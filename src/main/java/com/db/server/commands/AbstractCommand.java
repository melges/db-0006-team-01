package com.db.server.commands;

import com.db.domain.MessageTransport;

import java.io.IOException;

/**
 * This is an abstraction of server commands.
 *
 * @author ABogdanov
 * @author DNazarov
 * @author DPogorelov
 */
public abstract class AbstractCommand {
    protected MessageTransport messageTransport;

    protected AbstractCommand(MessageTransport messageTransport) {
        this.messageTransport = messageTransport;
    }

    /**
     * Performs actions required for the concrete command
     */
    public abstract void execute() throws IOException;
}
