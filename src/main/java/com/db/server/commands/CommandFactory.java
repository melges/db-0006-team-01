package com.db.server.commands;

import com.db.domain.MessageTransport;
import com.db.exceptions.CommandNotFoundException;
import com.db.server.*;

/**
 * Commands factory: analyzes command and constructs appropriate command object.
 *
 * @author ABogdanov
 * @author DNazarov
 * @author DPogorelov
 */
public class CommandFactory {

    /**
     * Creates command for the specified message
     *
     * @param messageTransport processing message
     * @param session client who asks for a command
     * @return created command
     */
    public static AbstractCommand createCommand(MessageTransport messageTransport, ChatServer server, ClientSession session)
            throws CommandNotFoundException {
        switch (messageTransport.getServerCommand()) {
            case SEND:
                return new MultiplyMessageCommand(messageTransport, session, server);
            case HISTORY:
                return new GetHistoryCommand(messageTransport, session, server);
            case LOGIN:
                return new LoginCommand(messageTransport, session);
            case ROOM:
                return new ChangeRoomCommand(messageTransport, session);
            default:
                break;
        }

        throw new CommandNotFoundException("Unknown command");
    }
}
