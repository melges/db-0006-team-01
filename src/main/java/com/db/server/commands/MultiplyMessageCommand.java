package com.db.server.commands;

import com.db.domain.MessageTransport;
import com.db.server.ChatServer;
import com.db.server.ClientSession;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Send message command command.
 *
 * @author ABogdanov
 * @author DNazarov
 * @author DPogorelov
 */
public class MultiplyMessageCommand extends AbstractCommand {
    private static final Logger LOGGER = Logger.getLogger(MultiplyMessageCommand.class.getName());

    private final ClientSession session;
    private final ChatServer server;

    protected MultiplyMessageCommand(MessageTransport messageTransport, ClientSession session, ChatServer server) {
        super(messageTransport);
        this.session = session;
        this.server = server;
    }

    @Override
    public void execute() throws IOException {
        try {
            messageTransport.setArgument(session.getClientName() + ">> ("
                    + new Date().toString() + "): " + messageTransport.getArgument());
            server.addMessage(messageTransport);
        } catch (InterruptedException e){
            LOGGER.warning("We was interrupted and can't write message");
            throw new IOException("Error on write message", e);
        }

    }
}

