package com.db.server.commands;

import com.db.domain.MessageTransport;
import com.db.server.ClientSession;

import java.io.IOException;

/**
 * Login command.
 *
 * @author ABogdanov
 * @author DNazarov
 * @author DPogorelov
 */
public class LoginCommand extends AbstractCommand {
    private final ClientSession session;

    LoginCommand(MessageTransport messageTransport, ClientSession session) {
        super(messageTransport);
        this.session = session;
    }

    @Override
    public void execute() throws IOException {
        messageTransport.setArgument("You have been already identified");
        session.sendMessageToClient(messageTransport);
    }
}
