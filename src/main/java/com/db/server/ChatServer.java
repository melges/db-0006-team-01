package com.db.server;

import com.db.domain.MessageTransport;
import com.db.server.workers.ClientWorker;
import com.db.server.workers.ServerWriter;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles incoming connections and creates new threads for each new client.
 *
 * @author ABogdanov
 * @author DNazarov
 * @author DPogorelov
 */
public class ChatServer implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(ChatServer.class.getName());

    public static final int DEFAULT_PORT = 2015;

    private ServerSocket serverSocket;

    ExecutorService executor = Executors.newCachedThreadPool();

    /**
     * We use this set for check is the user with specified name already registered and for
     */
    private final ConcurrentSkipListSet<ClientSession> loggedClients = new ConcurrentSkipListSet<>();

    private final ServerWriter defaultRoomWriter;

    public ChatServer() {
        defaultRoomWriter = new ServerWriter(loggedClients, this, new FileHistory("default"));
        defaultRoomWriter.start();
    }

    private void closeAllConnections() {
        for(ClientSession session : loggedClients)
            try {
                session.close();
            } catch (IOException e) {
                LOGGER.info("Some socket coulnd't close properly");
            }
    }

    public static void main(String[] args) {
        try {
            FileHandler fh = new FileHandler("logs/log.xml");
            LOGGER.addHandler(fh);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create file 'Database_log.xml'", e);
        }

        ChatServer chatServer = new ChatServer();
        Thread serverThread = new Thread(chatServer);
        serverThread.start();
    }

    public void shutdown() {
        try {
            serverSocket.close();
        } catch (IOException e) {

        }

        closeAllConnections();
    }

    @Override
    public void run() {
        try (ServerSocket s = new ServerSocket(DEFAULT_PORT)) {
            serverSocket = s;
            while (!Thread.interrupted()) {
                Socket session = null;
                ClientSession clientSession = null;
                try {
                    session = serverSocket.accept();
                    clientSession = new ClientSession(session);
                    executor.execute(new ClientWorker(clientSession, this));
                } catch (IOException e) {
                    LOGGER.log(Level.WARNING, "Could not create socket!", e);
                    // We don't need close clientSession because on exception in constructor it will be null
                    if(session != null) {
                        session.close();
                    }
                }
            }

            defaultRoomWriter.interrupt();
            closeAllConnections();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Could not create socket!", e);
        }
    }

    public String getHistory() {
        return defaultRoomWriter.getHistory();
    }

    public void addMessage(MessageTransport messageTransport) throws InterruptedException {
        defaultRoomWriter.addMessage(messageTransport);
    }

    public void addClient(ClientSession session) {
        loggedClients.add(session);
    }

    public void removeClient(ClientSession session) {
        loggedClients.remove(session);
    }
}