package com.db.domain;

import com.db.exceptions.CommandNotFoundException;

import java.util.HashMap;
import java.util.Map;

public enum Command {
    SEND("/snd"),
    HISTORY("/hist"),
    LOGIN("/chid"),
    EXIT("/exit"),
    ERROR("/err"),
    ROOM("/chroom");

    private String command;
    private static Map<String, Command> map;

    static {
        map = new HashMap<>();
        for (Command c : Command.values()) {
            map.put(c.command, c);
        }
    }

    Command(String command) {
        this.command = command;
    }

    public static Command getCommand(String command) throws CommandNotFoundException {
        Command c = map.get(command);
        if (c == null) {
            throw new CommandNotFoundException("Wrong command!");
        }
        return c;
    }
}
