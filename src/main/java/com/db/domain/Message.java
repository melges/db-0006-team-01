package com.db.domain;

import com.db.server.ClientSession;

import java.util.Date;

/**
 * Object represent messages sending from and to users
 */
public class Message {
    private String message;
    private Date date;
    private String senderName;

    public Message(String message, Date date, String senderName) {
        this.message = message;
        this.date = date;
        this.senderName = senderName;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getClientSession() {
        return senderName;
    }

    @Override
    public String toString() {
        return "<" + senderName + ">" + " [" + date + "] : " + message;
    }
}
