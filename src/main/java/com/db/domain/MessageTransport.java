package com.db.domain;

import com.db.domain.Command;

import java.io.Serializable;

public class MessageTransport implements Serializable {
    private Command serverCommand;

    private String argument;

    public MessageTransport(Command serverCommand, String argument) {
        this.serverCommand = serverCommand;
        this.argument = argument;
    }

    public Command getServerCommand() {
        return serverCommand;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        return "[" + getServerCommand() + " : " + getArgument() + "]";
    }
}
