package com.db.domain;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Student on 27.04.2015.
 */
public class User {
    private ObjectOutputStream writerToClient;
    private String name;

    public User(ObjectOutputStream writerToClient, String name) {
        this(name);
        this.writerToClient = writerToClient;
    }

    public User(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
