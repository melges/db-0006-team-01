package com.db.domain;

/**
 * Created by Dmitry Nazarov on 28.04.2015.
 */
public class Room {
    private String name;

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
