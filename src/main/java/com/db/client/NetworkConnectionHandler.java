package com.db.client;

import com.db.domain.MessageTransport;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Pogorelych on 27.04.2015.
 */
public class NetworkConnectionHandler implements ConnectionHandler{
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private Socket connectionSocket;
    private String serverURL;
    private int serverPort;
    private int maxReconnectTries = 10;

    public NetworkConnectionHandler(String serverURL, int serverPort) throws IOException, UnknownHostException {
        this.serverURL = serverURL;
        this.serverPort = serverPort;
        connectionSocket = new Socket(serverURL, serverPort);
        out = new ObjectOutputStream(connectionSocket.getOutputStream());
        in = new ObjectInputStream(connectionSocket.getInputStream());
    }

    @Override
    public void reconnect() throws IOException {
        System.out.println("Connection lost... trying reconnect");
        int i;
        for (i = 0; i < maxReconnectTries; i++) {
            try {
                connectionSocket = new Socket(serverURL, serverPort);
                in = new ObjectInputStream(connectionSocket.getInputStream());
                out = new ObjectOutputStream(connectionSocket.getOutputStream());
            }
            catch (IOException e) {
                System.out.println("Server unreachable... trying reconnect");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    return;
                }
                continue;
            }
            break;
        }
        if (i == 10) {
            throw new IOException("Couldn't reconnect to server");
        }
    }

    @Override
    public void close() throws IOException {
        in.close();
        out.close();
        connectionSocket.close();
    }

    @Override
    public MessageTransport receiveMessage() throws IOException, ClassNotFoundException {
            return (MessageTransport)in.readObject();
    }

    @Override
    public void sendMessage(MessageTransport message) throws IOException {
        try {
        out.writeObject(message);
        out.flush();
        } catch (IOException e) {
            System.out.println("Couldn't send message");
        }

    }
}
