package com.db.client;

import com.db.exceptions.CommandNotFoundException;
import com.db.domain.Command;
import com.db.domain.MessageTransport;
import com.db.exceptions.MessageTransportException;


public class ClientInputParser {
    private static final int MESSAGE_LIMIT = 150;
    private static final int NAME_LIMIT = 150;

    public MessageTransport parseString(String line) throws CommandNotFoundException, MessageTransportException{

        int delimiterIndex = line.indexOf(" ");
        String command;
        String argument;
        if (delimiterIndex < 0) {
            command = line;
            argument = "";
        } else {
            command = line.substring(0, delimiterIndex);
            argument = line.substring(delimiterIndex+1, line.length());
        }
        MessageTransport messageTransport = new MessageTransport(Command.getCommand(command),argument);
        switch (messageTransport.getServerCommand()){
            case SEND:
                if (messageTransport.getArgument().length() > MESSAGE_LIMIT) {
                    throw new MessageTransportException("Too long message");
                }
                break;
            case LOGIN:
                if (messageTransport.getArgument().length() > NAME_LIMIT) {
                    throw new MessageTransportException("Too long username");
                }
                if (messageTransport.getArgument().contains(" ")) {
                    throw new MessageTransportException("Invalid name");
                }
                break;
        }
        return messageTransport;
    }
}
