package com.db.client;

import com.db.domain.Command;
import com.db.exceptions.CommandNotFoundException;
import com.db.domain.MessageTransport;
import com.db.exceptions.MessageTransportException;

import java.io.*;
import java.net.Socket;
import java.util.logging.*;


public class Client {
    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private static final int CONSOLE_PORT = 2016;
    private static final int DEFAULT_SERVER_PORT = 2015;
    private static final String DEFAULT_SERVER = "localhost";
    private static final String SERVER_ARG = "-server";

    private static final String PORT_ARG = "-port";
    ConnectionHandler connectionHandler;
    BufferedReader consoleIn;

    public Client(ConnectionHandler connectionHandler, BufferedReader consoleIn) {
        if (connectionHandler == null) {
            throw new IllegalArgumentException("connection handler cannot be null");
        }

        if (consoleIn == null) {
            throw new IllegalArgumentException("console input cannot be null");
        }

        this.connectionHandler = connectionHandler;
        this.consoleIn = consoleIn;
    }


    public void launchClient() {
        ServerListener serverListener = new ServerListener(connectionHandler);
        System.out.println("You're successfully connected to server");
        serverListener.start();
        ClientInputParser clientInputParser = new ClientInputParser();
        String line;
        while (true) {
            try {
                line = consoleIn.readLine();
                MessageTransport message;
                try {
                    message = clientInputParser.parseString(line);
                    if (Command.EXIT == message.getServerCommand()) {
                        LOGGER.log(Level.INFO, "stopping client...");
                        break;
                    }
                    connectionHandler.sendMessage(message);
                } catch (CommandNotFoundException e) {
                    LOGGER.log(Level.INFO, "Invalid command", e);
                } catch (MessageTransportException e){
                    LOGGER.log(Level.INFO, "Invalid argument for comand", e);
                }
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Problem with input console", e);
                break;
            }
        }

        try {
            serverListener.stopListener();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "error at exit", e);
        }
    }

    public static void main(String[] args) {
        try {
            FileHandler fh = new FileHandler("logs/clientlog.xml");
            LOGGER.addHandler(fh);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create log file", e);
        }

        String server = DEFAULT_SERVER;
        int port = DEFAULT_SERVER_PORT;
        for (int i = 0; i < args.length; ++i) {
            if (SERVER_ARG.equals(args[i]) && (i + 1 < args.length)) {
                ++i;
                server = args[i];
            } else if (PORT_ARG.equals(args[i]) && (i + 1 < args.length)) {
                ++i;
                try {
                    port = Integer.parseInt(args[i]);
                } catch (NumberFormatException e) {
                    LOGGER.log(Level.SEVERE, "Incorrect port number", e);
                }
            }
        }

        try (
                ConnectionHandler connectionHandler = new NetworkConnectionHandler(server, port);
                Socket consoleConnection = new Socket("localhost", CONSOLE_PORT);
                BufferedReader consoleIn = new BufferedReader(
                        new InputStreamReader(consoleConnection.getInputStream()))) {
            Client client = new Client(connectionHandler, consoleIn);
            client.launchClient();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Couldn't connect to server or console");
        }
    }
}

