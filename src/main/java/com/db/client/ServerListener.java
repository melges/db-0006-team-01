package com.db.client;

import com.db.domain.MessageTransport;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Student on 27.04.2015.
 */
public class ServerListener extends Thread {
    ConnectionHandler connectionHandler;
    private static final Logger LOGGER = Logger.getLogger(ServerListener.class.getName());

    public ServerListener(ConnectionHandler connectionHandler) {
        super();
        this.connectionHandler = connectionHandler;

    }

    @Override
    public void run() {
        while (true) {
            try {
                MessageTransport message;
                message = connectionHandler.receiveMessage();

                if (message.getServerCommand() == null) {
                    System.out.println("Server:> " + message.getArgument());
                }
                else {
                    switch (message.getServerCommand()) {
                        case ERROR:
                            System.out.println("Server:> " + message.getArgument());
                            break;
                        case HISTORY:
                            System.out.println("Message history for this room:\n" + message.getArgument());
                            break;
                        case LOGIN:
                            if (message.getArgument() == null){
                                System.out.println("Login failed");
                            } else {
                                System.out.println("You're logined as " + message.getArgument());
                            }
                            break;
                        case SEND:
                            System.out.println(">" + message.getArgument());
                            break;
                        default:
                            System.out.println(message.getArgument());
                            break;
                    }
                }
            } catch (ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Critical error: Incompatible protocol", e);
                break;
            } catch (IOException e) {
                if (Thread.interrupted()) {
                    LOGGER.log(Level.INFO, "exiting");
                    break;
                }
                else {
                    try {
                        LOGGER.log(Level.SEVERE, "Connection Lost");
                        connectionHandler.reconnect();
                    } catch (IOException e1) {
                        break;
                    }
                }
            }
        }

    }

    public void stopListener() throws IOException {
        this.interrupt();
        connectionHandler.close();
    }
}
