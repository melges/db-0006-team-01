package com.db.client;

import com.db.domain.MessageTransport;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by Pogorelych on 27.04.2015.
 */
public interface ConnectionHandler extends Closeable {

    void close() throws IOException;
    MessageTransport receiveMessage() throws IOException, ClassNotFoundException;
    void sendMessage(MessageTransport message) throws IOException;
    void reconnect() throws IOException;
}
