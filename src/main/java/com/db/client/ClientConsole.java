package com.db.client;

import java.io.*;
import java.net.ServerSocket;
import java.util.logging.*;

/**
 * Created by Student on 28.04.2015.
 */
public class ClientConsole {
    private static final Logger LOGGER = Logger.getLogger(ClientConsole.class.getName());

    public static void main(String[] args) {
        try(ServerSocket serverSocket = new ServerSocket(2016);
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                    (serverSocket.accept()).getOutputStream()))
        ){
            String line = null;
            while (!"/exit".equals(line)){
                line = in.readLine();
                out.write(line + "\n");
                out.flush();
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Client console fail");
        }
    }
}
