package com.db;

import com.db.domain.Message;
import com.db.domain.Room;
import com.db.domain.User;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Dmitry Nazarov on 27.04.2015.
 */
public class DatabaseConnector {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class.getName());

    static {
        try {
            FileHandler fh = new FileHandler("logs/Database_log.xml");
            LOGGER.addHandler(fh);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Couldn't create file 'Database_log.xml'", e);
        }

        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Not Found Database Client Driver!", e);
        }
    }


    public static void main(String[] args) {
        DatabaseConnector databaseConnector = new DatabaseConnector();

        User user = new User("Ivan44444");
        databaseConnector.insertUser(user);
    }


    private static final String PATH_TO_DATABASE = "jdbc:derby://localhost/team02";

    private static final String SQL_SELECT_ID_USER_BY_NAME = "SELECT id_user FROM users WHERE name = ?";
    private static final String SQL_SELECT_ID_ROOM_BY_NAME = "SELECT id_room FROM rooms WHERE name = ?";
    private static final String SQL_SELECT_MESSAGES_FOR_ROOM = "SELECT name, date, message " +
            "FROM messages, users" +
            "WHERE messages.id_message = users.id_user " +
            "AND id_room = ?" +
            "ORDER BY messages.id_message";

    private static final String SQL_INSERT_NEW_USER = "INSERT INTO users(name) VALUES(?)";
    private static final String SQL_INSERT_NEW_MESSAGE = "INSERT INTO messages(date, message, id_user) VALUES(?, ?, ?)";

    private static final String FIELD_ID_USER_FROM_TABLE_USERS = "id_user";
    private static final String FIELD_NAME_FROM_TABLE_USERS = "name";
    private static final String FIELD_MESSAGE_FROM_TABLE_MESSAGE = "message";
    private static final String FIELD_DATE_FROM_TABLE_MESSAGE = "date";
    private static final String FIELD_ID_ROOM_FROM_TABLE_ROOMS = "id_room";

    public void insertUser(User user) {
        try (Connection conn = DriverManager.getConnection(PATH_TO_DATABASE)) {
            PreparedStatement insertNewUserStm = conn.prepareStatement(SQL_INSERT_NEW_USER);
            insertNewUserStm.setString(1, user.toString());
            insertNewUserStm.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Could not insert new User at Table Users!", e);
        }
    }

    public void insertMessage(Message message) {
        try (Connection conn = DriverManager.getConnection(PATH_TO_DATABASE)) {
            PreparedStatement insertNewUserStm = conn.prepareStatement(SQL_INSERT_NEW_USER);
            insertNewUserStm.setString(1, message.getClientSession());
            insertNewUserStm.executeUpdate();

            PreparedStatement selectUserIdStm = conn.prepareStatement(SQL_SELECT_ID_USER_BY_NAME);
            selectUserIdStm.setString(1, message.getClientSession());
            int idClient = selectUserIdStm.executeQuery().getInt(FIELD_ID_USER_FROM_TABLE_USERS);

            PreparedStatement insertNewMessageStm = conn.prepareStatement(SQL_INSERT_NEW_MESSAGE);
            insertNewMessageStm.setDate(1, new java.sql.Date(message.getDate().getTime()));
            insertNewMessageStm.setString(2, message.getMessage());
            insertNewMessageStm.setInt(3, idClient);
            insertNewMessageStm.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Could not insert new Message at Table Messages!", e);
        }
    }

    public List<Message> selectMessages(Room room) {
        List<Message> messages = new ArrayList<Message>();

        try (Connection conn = DriverManager.getConnection(PATH_TO_DATABASE)) {
            PreparedStatement selectRoomIdStm = conn.prepareStatement(SQL_SELECT_ID_ROOM_BY_NAME);
            selectRoomIdStm.setString(1, room.toString());
            int idRoom = selectRoomIdStm.executeQuery().getInt(FIELD_ID_ROOM_FROM_TABLE_ROOMS);

            PreparedStatement selectMessagesForRoomStm = conn.prepareStatement(SQL_SELECT_MESSAGES_FOR_ROOM);
            selectMessagesForRoomStm.setInt(1, idRoom);
            ResultSet resultSet = selectMessagesForRoomStm.executeQuery();

            while (resultSet.next()) {
                Message message = new Message(resultSet.getString(FIELD_MESSAGE_FROM_TABLE_MESSAGE),
                        resultSet.getDate(FIELD_DATE_FROM_TABLE_MESSAGE),
                        resultSet.getString(FIELD_NAME_FROM_TABLE_USERS));
                messages.add(message);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Could not select Messages from Table Messages!", e);
        }
        return messages;
    }
}