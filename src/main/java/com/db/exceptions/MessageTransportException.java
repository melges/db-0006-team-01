package com.db.exceptions;

/**
 * Created by Student on 28.04.2015.
 */
public class MessageTransportException extends Exception {
    public MessageTransportException() {
        super();
    }

    public MessageTransportException(String message) {
        super(message);
    }

    public MessageTransportException(String message, Throwable cause) {
        super(message, cause);
    }

}
