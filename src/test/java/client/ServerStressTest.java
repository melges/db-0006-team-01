package client;

import com.db.domain.Command;
import com.db.domain.MessageTransport;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class ServerStressTest {
    private static final String logFile = "stresstest.log";
    private static final Logger log = Logger.getLogger(ServerStressTest.class.getName());

    public static void main(String[] args) {
        try {
            FileHandler handler = new FileHandler(logFile, true);
            log.addHandler(handler);
        } catch (IOException e) {
            throw new IllegalStateException("Could not add log file handler.", e);
        }

        ServerStressTest test = new ServerStressTest();
        test.start(1000, args[0], 30);
    }

    public void start(int clientNumber, String host, int seconds) {
        if (clientNumber <= 0) {
            throw new IllegalArgumentException("Incorrect number of clients: " + clientNumber);
        }

        ExecutorService exec = Executors.newFixedThreadPool(clientNumber);
        List<TestClient> testers = new LinkedList<>();
        Random random = new Random(System.currentTimeMillis());
        for(int i = 0; i < clientNumber; i++) {
            TestClient tester = new TestClient(host, "Client " + random.nextInt());
            testers.add(tester);
            exec.execute(tester);
            try {
                Thread.sleep((random.nextInt() >>> 1) % 25);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    private class TestClient extends Thread {
        String host;
        String name;
        double average;
        boolean error;
        Random r = new Random(System.currentTimeMillis());

        public TestClient(String host, String name) {
            this.host = host;
            this.name = name;
        }

        @Override
        public void run() {
            long counter = 1;
            long timeUsed = 0;

            try (Socket socket = new Socket(host, 2015);
                 ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {
                MessageTransport msgHello = new MessageTransport(Command.LOGIN, name);
                MessageTransport msgTest;

                out.writeObject(msgHello);
                out.reset();
                out.flush();
                getResponse(in);

                while (true) {
                    msgTest = new MessageTransport(Command.SEND, "Test message " + name + " " + counter );
                    out.writeObject(msgTest);
                    out.reset();
                    out.flush();
                    long start = System.currentTimeMillis();
                    MessageTransport response;
                    do {
                        response = getResponse(in);
                    } while (!response.getArgument().contains(msgTest.getArgument()));
                    long finish = System.currentTimeMillis();
                    counter++;
                    timeUsed += finish - start;
                    Thread.sleep(1000 + (r.nextInt() >>> 1) % 3000);
                }
            } catch (IOException | ClassNotFoundException | ClassCastException |
                    NullPointerException e) {
                // log.warning("There are error " + e.getMessage());
                error = true;
            } catch (InterruptedException ignored) {

            }

            average = timeUsed / counter;
        }

        private MessageTransport getResponse(ObjectInputStream in) throws IOException, ClassNotFoundException, InterruptedException {
            MessageTransport msg = null;
            do {
                Object received = in.readObject();
                if (MessageTransport.class.equals(received.getClass())) {
                    msg = (MessageTransport)received;
                    break;
                }
                else {
                    log.info(name + ": incorrect object received");
                }

                Thread.sleep(10);
            } while (true);

            return msg;
        }
    }
}