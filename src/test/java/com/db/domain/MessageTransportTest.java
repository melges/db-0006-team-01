package com.db.domain;

import org.junit.Test;
import static org.fest.assertions.Assertions.*;
/**
 * Created by Student on 28.04.2015.
 */
public class MessageTransportTest {
    @Test
    public void shouldNotChangeFieldsWhenChange() {
        Command commandDummy = Command.SEND;
        String argumentDummy = "arg";
        MessageTransport sut = new MessageTransport(commandDummy, argumentDummy);
        commandDummy = null;
        argumentDummy = argumentDummy + "new";
        assertThat(sut.getArgument()).isEqualTo("arg");
        assertThat(sut.getServerCommand()).isEqualTo(Command.SEND);
    }
}
