package com.db.domain;

import com.db.exceptions.CommandNotFoundException;
import org.junit.Test;
import static org.fest.assertions.Assertions.*;

/**
 * Created by Student on 28.04.2015.
 */
public class CommandTest {
    @Test(expected = CommandNotFoundException.class)
    public void shouldThrowExceptionWhenThereIsNoCommand() throws CommandNotFoundException {
        Command sut = Command.getCommand("/lala");
    }

    @Test
    public void shouldGetCommandWhenCommandExists() throws CommandNotFoundException {
        Command sut = Command.getCommand("/snd");
        assertThat(sut).isEqualTo(Command.SEND);
    }
}
