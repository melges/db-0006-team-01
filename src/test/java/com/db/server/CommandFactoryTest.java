package com.db.server;

import com.db.client.Client;
import com.db.domain.Command;
import com.db.domain.MessageTransport;
import com.db.domain.User;
import com.db.exceptions.CommandNotFoundException;
import com.db.server.commands.CommandFactory;
import com.db.server.commands.GetHistoryCommand;
import com.db.server.commands.LoginCommand;
import com.db.server.commands.MultiplyMessageCommand;
import org.junit.Test;

import java.io.ObjectOutputStream;

import static org.fest.assertions.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Created by Student on 28.04.2015.
 */
public class CommandFactoryTest {
    @Test
    public void shouldReturnRightInstanceWhenCreateCommand() throws CommandNotFoundException{
        ChatServer chatServerMock = mock(ChatServer.class);
        ObjectOutputStream oosMock = mock(ObjectOutputStream.class);
        ClientSession userMock = mock(ClientSession.class);

        MessageTransport sendMessage = new MessageTransport(Command.SEND, "");
        MessageTransport historyMessage = new MessageTransport(Command.HISTORY, "");
        MessageTransport loginMessage = new MessageTransport(Command.LOGIN, "");

        assertThat(CommandFactory.createCommand(sendMessage, chatServerMock, userMock))
                .isInstanceOf(MultiplyMessageCommand.class);
        assertThat(CommandFactory.createCommand(historyMessage, chatServerMock, userMock))
                .isInstanceOf(GetHistoryCommand.class);
        assertThat(CommandFactory.createCommand(loginMessage, chatServerMock, userMock))
                .isInstanceOf(LoginCommand.class);
    }
}
