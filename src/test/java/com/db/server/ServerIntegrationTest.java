package com.db.server;

import com.db.client.Client;
import com.db.client.ConnectionHandler;
import com.db.client.NetworkConnectionHandler;
import org.junit.Test;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;

/**
 * Created by Student on 29.04.2015.
 */
public class ServerIntegrationTest {
    @Test
    public void mainFlowTest() throws InterruptedException, IOException {
        ChatServer chatServer = new ChatServer();
        Thread serverThread = new Thread(chatServer);
        serverThread.start();

        Thread.sleep(3000);

        ServerSocket serverSocket = new ServerSocket(2016);

        new Thread(() -> {
            ConnectionHandler connectionHandler = null;
            try {
                Socket consoleConnection = new Socket("localhost", 2016);
                BufferedReader consoleIn = new BufferedReader(
                        new InputStreamReader(consoleConnection.getInputStream()));
                connectionHandler = new NetworkConnectionHandler("localhost", 2015);
                Client client = new Client(connectionHandler, consoleIn);
                client.launchClient();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        BufferedWriter out = new BufferedWriter(new OutputStreamWriter((serverSocket.accept()).getOutputStream()));
        Thread.sleep(3000);

        out.write("/chid Melges\n");
        out.flush();
        out.write("/snd Message\n");
        out.flush();
        out.write("exit");
        out.flush();

        chatServer.shutdown();
    }
}
